﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidPohjalikumalt
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene i = new Inimene("Henn");
            Inimene t = new Inimene("Maris");

            if (i.Abielu(t))
            {
                Console.WriteLine($"{i.Nimi} kaasaks on {i.Kaasa.Nimi}");
            }
            else
            {
                Console.WriteLine("äi lähe mette");
            }

            Inimene ml = i.UusLaps("Mai-Liis");
            Inimene pr = i.UusLaps("Pille-Riin");

            Console.WriteLine(pr.Isa.Kaasa.Lapsed[0].Nimi);

        }
    }

    class Inimene
    {
        private string _Nimi;
        private DateTime _Sünniaeg;

        public Inimene Kaasa = null;
        public List<Inimene> Lapsed = new List<Inimene>();
        public Inimene Isa = null;
        public Inimene Ema = null;

        public Inimene(string nimi)
        {
            _Nimi = nimi;
        }

        public int AnnaVanus()  // funktsioon
        {
            int v = DateTime.Now.Year - this.Sünniaeg.Year;
            if (Sünniaeg.AddYears(v).Date > DateTime.Today) v--;
            return v;
        }
        public static int AnnaVanus(Inimene see)  // funktsioon
        {
            int v = DateTime.Now.Year - see.Sünniaeg.Year;
            if (see.Sünniaeg.AddYears(v).Date > DateTime.Today) v--;
            return v;
        }


        public int Vanus    // property
        {
            get
            {
                int v = DateTime.Now.Year - Sünniaeg.Year;
                if (Sünniaeg.AddYears(v).Date > DateTime.Today) v--;
                return v;
            }
        }

        public string Nimi
        {
            get => _Nimi;
            // get { return _Nimi;}
            internal set => _Nimi = value.Substring(0,1).ToUpper() 
                         + value.Substring(1).ToLower() ;
            //set { _Nimi = value; }
            
        }
        public DateTime Sünniaeg
        {
            get => _Sünniaeg;
            set => _Sünniaeg = value > DateTime.Today ? DateTime.Today : value;
        }

        public bool Abielu(Inimene kaasa)
        {
            if (this.Kaasa == null && kaasa.Kaasa == null)
            {
                this.Kaasa = kaasa;
                kaasa.Kaasa = this;
                return true;
            }
            else return false;
        }

        public bool Lahutus()
        {
            if (this.Kaasa == null) return false;
            Kaasa.Kaasa = null;
            this.Kaasa = null;
            return true;
        }

        public Inimene UusLaps(string nimi)
        {
            Inimene uus = new Inimene(nimi);
            this.Lapsed.Add(uus);
            uus.Isa = this;
            if (this.Kaasa != null)
            {
                this.Kaasa.Lapsed.Add(uus);
                uus.Ema = this.Kaasa;
            }
            return uus;
        }


        public override string ToString()
        {
            return ($"Inimene {_Nimi} vanusega {Vanus}" );
        }
    }
}
